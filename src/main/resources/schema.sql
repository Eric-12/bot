DROP TABLE IF EXISTS message;
CREATE TABLE message (
     id INT AUTO_INCREMENT PRIMARY KEY,
     question TEXT,
     reponse TEXT
);

DROP TABLE IF EXISTS bot;
CREATE TABLE bot (
  id INT AUTO_INCREMENT PRIMARY KEY,
  pseudo VARCHAR(250),
  phrase TEXT,
  is_valid BOOLEAN,
  created_at DATE
);

DROP TABLE IF EXISTS crazy_bot;
CREATE TABLE crazy_bot (
     id INT AUTO_INCREMENT PRIMARY KEY,
     reponse VARCHAR(250)
);