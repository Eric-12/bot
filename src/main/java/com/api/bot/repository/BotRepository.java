package com.api.bot.repository;

import com.api.bot.model.BotEntity;
import com.api.bot.model.MessageEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository  // @Repository est une spécialisation de @Component.
public interface BotRepository extends CrudRepository<BotEntity, Long> {

    // Methodes pour recuperer le premier enregistrement d'une table apres un save ou un update par exemple
    BotEntity findTopByOrderByIdAsc();

    // Récupère le dernier enregistrement d'une table (apres un save ou un update).
    BotEntity findTopByOrderByIdDesc();
    BotEntity findFirstByOrderByIdDesc();
}

