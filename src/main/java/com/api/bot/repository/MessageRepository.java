package com.api.bot.repository;

import com.api.bot.model.MessageEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// @Repository annotation Spring pour indiquer que la classe est un bean, et que son rôle est de communiquer avec notre source de données
@Repository  // @Repository est une spécialisation de @Component.
public interface MessageRepository extends CrudRepository<MessageEntity, Long> {

    MessageEntity findByQuestion(String question);

}

