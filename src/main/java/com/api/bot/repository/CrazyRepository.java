package com.api.bot.repository;

import com.api.bot.model.BotEntity;
import com.api.bot.model.CrazyEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository  // @Repository est une spécialisation de @Component.
public interface CrazyRepository extends CrudRepository<CrazyEntity, Long> {

}