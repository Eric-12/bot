package com.api.bot.controller;

import com.api.bot.model.WelcomeModel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;


@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class HomeController {
    //@Autowired
    final WelcomeModel welcomeModel;

    @GetMapping()
    public String getMessageHome(){
        return welcomeModel.toString();
    }

    @PostMapping()
    public String connect(@RequestBody String value){
        LocalDate localedate = LocalDate.now();
        String response = value + localedate.toString();
        System.out.println(response);
        return response;
    }
}
