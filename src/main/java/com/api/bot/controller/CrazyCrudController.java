package com.api.bot.controller;

import com.api.bot.model.CrazyEntity;
import com.api.bot.repository.CrazyRepository;
import com.api.bot.service.CrazyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/crazy-bot")
public class CrazyCrudController {

    private final CrazyService crazyService;

    @PostMapping()
    public void createMessageCrazyBot(@RequestBody CrazyEntity crazyEntity){
        crazyService.save(crazyEntity);
    }

    @GetMapping("/responses")
    public Iterable <CrazyEntity> getMessagesCrazyBot(){
        return crazyService.getMessagesCrazyBot();
    }
}
