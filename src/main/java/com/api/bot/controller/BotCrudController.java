package com.api.bot.controller;

import com.api.bot.model.BotEntity;
import com.api.bot.service.MessageService;
import com.api.bot.service.BotService;
import com.api.bot.service.dto.QuestionDto;
import com.api.bot.service.dto.ReponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RequiredArgsConstructor // lombok
@RestController
@RequestMapping(value = "api")
public class BotCrudController {

    //@Autowired
    final BotService botService;
    final MessageService messageService;

    // read all users
    @GetMapping("/bots")
    public Iterable<BotEntity> getMessagesBots() {

        return botService.getAllBot();
    }

    // read one user
    @GetMapping("/bot/{id}")
    public Optional<BotEntity> getMessageBotById(@PathVariable("id") long id) {
        return botService.getBotById(id);
    }

    // update message
    @PutMapping("/bot/{id}")
    public void updateMessageBot(@PathVariable("id") long id) {
        //botService.saveUser();
    }

    // Poser une question au bot et recevoir une réponse
    @PostMapping("/bot")
    public ReponseDto createMessageBot(@RequestBody QuestionDto messageQuestionDto) {
        return botService.replyToMessage(messageQuestionDto);
    }

    // poser une question au bot et recevoir une reponse.
   /* @PostMapping("/bot")
    public Map<String, String> replyMessage(@RequestBody BotEntity botEntity ) {
        return botService.replyToMessage(botEntity);
    }*/

}
