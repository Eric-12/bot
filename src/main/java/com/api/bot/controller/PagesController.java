package com.api.bot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("pages")
public class PagesController {

    @GetMapping("/")
    public String home(){
        return "pages/home";
    }

    @GetMapping("/contact")
    public String contact(@RequestParam(required = false, defaultValue = "ContactPage") String name, ModelMap modelMap){
        System.out.println(name);
        /*
        Sans @RequestParam
        Pour récupère les paramétres d'une requete GET
            import javax.servlet.http.HttpServletRequest;

            public String contact(HttpServletRequest request){
                request.getParameter("name");
            }
        */
        modelMap.put("name",name);
        return "pages/contact";
    }

}
