package com.api.bot.controller;

import com.api.bot.model.MessageEntity;
import com.api.bot.service.MessageService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * CRUD (Create, Read, Update, Delete)
 * Verbes pour requêter via le protocol http
 */

// lombok
@Data
@RequiredArgsConstructor
@RestController//indique à Spring que c'est un bean et d’insérer le retour de la méthode au format JSON dans le corps de la réponse HTTP.
@RequestMapping("api")
public class MessageCrudController {

    private final MessageService messageService;

    /*public MessageCrudController(MessageService messageService) {
        this.messageService = messageService;
    }*/

    // read all messages
    @GetMapping("/messages")
    public Iterable<MessageEntity> getAllMessages(){

        return messageService.getMessages();
    }

    // read one message
    @GetMapping("/message/{id}")
    // @ResponseBody pas necessaire car @restController (rest echange de donnée au frmat Json
    public Optional<MessageEntity> getMessageById(@PathVariable("id") long id){
        return messageService.getMessageById(id);
    }

    // create one message
    @PostMapping("/message")
    public void addMessage(@RequestBody MessageEntity messageEntity){
        messageService.saveMessage(messageEntity);
    }

    // update message
    @PutMapping("/message/{id}")
    public void updateMessage(@PathVariable("id") long id){
        //messageService.saveMessage(messageEntity);
    }

    //delete message
    @DeleteMapping("/message/{id}")
    public void deleteMessage(@PathVariable("id") Long id){
        messageService.deleteMessage(id);
    }


}
