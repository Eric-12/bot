package com.api.bot;

import com.api.bot.model.WelcomeModel;
import com.api.bot.service.BotService;
import com.api.bot.service.WelcomeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;
@RequiredArgsConstructor // lombok
@SpringBootApplication
public class BotApplication implements CommandLineRunner {

	// @Autowired : auto initialisation par Spring de la classe WelcomeModel
	private final WelcomeModel welcomeModel;
	private final WelcomeService welcomeService;
	private final BotService bts;

	public static void main(String[] args){
		SpringApplication.run(BotApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		WelcomeModel welcomeModel = welcomeService.getWelcomeModel();
		System.out.println(welcomeModel.toString());

		//bts.crazy();
	}
}
