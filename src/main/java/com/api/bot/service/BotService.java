package com.api.bot.service;

import com.api.bot.model.BotEntity;
import com.api.bot.model.CrazyEntity;
import com.api.bot.model.MessageEntity;
import com.api.bot.repository.BotRepository;
import com.api.bot.repository.CrazyRepository;
import com.api.bot.service.dto.QuestionDto;
import com.api.bot.service.dto.ReponseDto;
import com.api.bot.service.mapper.Mapping;
import lombok.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.Random;

@Data
@Service
@RequiredArgsConstructor
// lombok
public class BotService {

    // injection de dépendances (délègue l'instanciation des classes a un tiers)
    private final BotRepository botRepository;
    private final MessageService messageService;
    private final CrazyRepository crazyRepository;

    private final Random randomize = new Random();
    private int instance = 1;


/*    public BotService(){
        System.out.println(instance);
    }*/

    public Iterable<BotEntity> getAllBot() {
        return botRepository.findAll();
    }

    public Optional<BotEntity> getBotById(Long id) {
        return botRepository.findById(id);
    }

    public void deleteMessage(final Long id) {
        botRepository.deleteById(id);
    }

    public void save(@RequestBody BotEntity botEntity) {
        botRepository.save(botEntity);
    }


    /*  METHODE avec classes  */
    public ReponseDto replyToMessage(QuestionDto questionDto) {
        ReponseDto reponseDto = new ReponseDto();

        // Génère un nombre >= 0 et < 3
        int randomValue = randomize.nextInt(3);

        // si le nombre généré est == 1 je renvoie un message désagréable
        if (randomValue == 1) {
            setMessageCrasyBot(reponseDto);
        } else {
            // sinon je renvoie un message normal
            setMessageNormalBot(questionDto, reponseDto);
        }

        // Enregistre dans la table bot les infos transmises par l'utilisateur
        persistMessage(questionDto);
        BotEntity lastId = botRepository.findFirstByOrderByIdDesc();

        // Formatage de la date
        String formatDateToString = lastId.getCreatedAt().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
        reponseDto.setTimestamp(formatDateToString);

        // la class reponseDto permet de conserver les reponses qui seront renvoyé a l'utilisateur
        reponseDto.setPseudo(questionDto.getPseudo());

        System.out.println(" Pseudo : " + reponseDto.getPseudo() + "\n Réponse du bot : " + reponseDto.getResponse() + "\n Date et heure formattée : " + reponseDto.getTimestamp());

        return reponseDto; // recupere le dernier enregistrement de la table bot.

    }

    private void persistMessage(QuestionDto questionDto) {
        // Persistance des données
        System.out.println(instance++);
        // Mapping mapping = new Mapping(questionDto.getPseudo(), questionDto.getPhrase(), questionDto.getIsValid());
        Mapping mapping = new Mapping();
        this.save(mapping.convertToBotEntity(questionDto));
    }

    private void setMessageNormalBot(QuestionDto questionDto, ReponseDto reponseDto) {
        MessageEntity messageEntity = messageService.getQuestion(questionDto.getPhrase());
        questionDto.setIsValid(false);
        if (messageEntity == null) {
            reponseDto.setResponse("Désolé " + questionDto.getPseudo() + ", je n'ai pas compris votre question !");
        } else {
            reponseDto.setResponse(messageEntity.getReponse() + ' ' + questionDto.getPseudo());
        }
    }

    private void setMessageCrasyBot(ReponseDto reponseDto) {
        // Recupere le nombre d'enregistrement
        long countCrazyEntities = crazyRepository.count();

        // Récupère une reponse aléatoire en générant un nombre en fct du nombre d'enregistrement
        Random random = new Random();
        int r = ((int) countCrazyEntities);
        int id = random.nextInt(r);

        Optional<CrazyEntity> crazyEntity = crazyRepository.findById((long) id);
        if (crazyEntity.isPresent()) {
            reponseDto.setResponse(crazyEntity.get().getReponse());
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof BotService;
    }


    /*  METHODE HashMap

    public Map<String, String> replyToMessage(BotEntity botEntity) {

        // implementation de Map avec HashMap pour le type json
        Map<String, String> json = new HashMap<>();

        // Nombre >= 0 et < 3
        int randomValue = randomize.nextInt(3);
        System.out.println(randomValue);

        // si le nombre généré est == 1 je renvoie un message désagréable
        if (randomValue == 1) {
            json = this.getCrasyMessage();
        } else {
            json = this.getNormalMessage(botEntity,json);
        }

        // Ajoute la date de connection utilisateur
        botEntity.setCreatedAt(LocalDateTime.now());
        // Persistance des données
        this.save(botEntity);

        json.put("timestamp", LocalDateTime.now().toString());
        return json;
    }

     private Map<String, String> getCrasyMessage() {
        // recupere le nombre d'enregistrement
        long countCrazyEntities = crazyRepository.count();

        // implementation de Map avec HashMap
        Map<String, String> json = new HashMap<>();

        // Récupère une reponse aléatoire en générant un nombre en fct du nombre d'enregistrement
        Random random = new Random();
        int r = ((int) countCrazyEntities);
        int id = random.nextInt(r);

        Optional<CrazyEntity> crazyEntity = crazyRepository.findById((long) id);

        if (crazyEntity.isPresent()) {
            System.out.println(crazyEntity.get().getReponse());
            String response = crazyEntity.get().getReponse();
            json.put("reponse", response);
            return json;
        }

        json.put("reponse", "une erreur c'est produite !");
        return json;
    }

    private Map<String, String> getNormalMessage(BotEntity botEntity, Map<String, String> json){
        // Sinon un message pré enregistré
        // requete avec l'orm pour parcourir tous les message where "question" = botEntity.getPhrase()
        MessageEntity messageEntity = messageService.getQuestion(botEntity.getPhrase());

        if (messageEntity == null) {
            botEntity.setIsValid(false);
            json.put("response", "Désolé " + botEntity.getPseudo() + ", je n'ai pas compris votre question !");
            System.out.println(json);
        } else {
            botEntity.setIsValid(false);
            System.out.println(messageEntity.getReponse());
            json.put("response", messageEntity.getReponse() + ' ' + botEntity.getPseudo());
            System.out.println(json);
        }
        return json;
    }

    */
}
