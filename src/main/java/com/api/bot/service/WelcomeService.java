package com.api.bot.service;

import com.api.bot.model.WelcomeModel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Data
@RequiredArgsConstructor // lombok
public class WelcomeService {

    private final WelcomeModel welcomeModel;
}
