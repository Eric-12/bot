package com.api.bot.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
public class ReponseDto {
    private String pseudo;
    private String response;
    private String timestamp;
}
