package com.api.bot.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Setter
@Getter
public class QuestionDto {
    private String pseudo;
    private String phrase;
    private Boolean isValid;

/*    public QuestionDto( Optional<String> pseudo, Optional<String> phrase ){
        this.pseudo = pseudo.isPresent() && pseudo.get().length()!=0 ? pseudo.get().trim() : "error";
        this.phrase = phrase.isPresent() && phrase.get().length()!=0 ? phrase.get().trim() : "error";
    }*/

    public QuestionDto( String pseudo, String phrase ) {
        this.pseudo = !pseudo.isEmpty() || !pseudo.isBlank() ? pseudo.trim() : "error";
        this.phrase = !phrase.isEmpty() || !phrase.isBlank() ? phrase.trim() : "error";
    }
}
