package com.api.bot.service;

import com.api.bot.model.CrazyEntity;
import com.api.bot.model.MessageEntity;
import com.api.bot.repository.CrazyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
@RequiredArgsConstructor
public class CrazyService {

    private final CrazyRepository crazyRepository;

    public CrazyEntity save(@RequestBody CrazyEntity crazyEntity) {
        return crazyRepository.save(crazyEntity);
    }

    public Iterable <CrazyEntity> getMessagesCrazyBot(){
         return crazyRepository.findAll();
    }
}
