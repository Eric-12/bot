package com.api.bot.service;

import com.api.bot.model.MessageEntity;
import com.api.bot.repository.MessageRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;


@Data // Annotation Lombok : génére les getters et setters
@Service  // @Service est une spécialisation de @Component
@RequiredArgsConstructor // lombok
public class MessageService {
    private final MessageRepository messageRepository;

    public MessageEntity getQuestion(String question){
        return messageRepository.findByQuestion(question);
    }

    public Iterable<MessageEntity> getMessages() {

        return messageRepository.findAll();
    }

    // Optional : encapsuler un objet dont la valeur peut être null
    public Optional<MessageEntity> getMessageById(final Long id) {

        return messageRepository.findById(id);
    }

    public void deleteMessage(final Long id) {
        messageRepository.deleteById(id);
    }

    public MessageEntity saveMessage(@RequestBody MessageEntity messageEntity) {
        return messageRepository.save(messageEntity);
    }


}
