package com.api.bot.service.mapper;

import com.api.bot.model.BotEntity;
import com.api.bot.service.dto.QuestionDto;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

// @RequiredArgsConstructor
public class Mapping {
/*
    private String pseudo;
    private String phrase;
    private Boolean isValid;
*/
    //private BotEntity botEntity = new BotEntity;

/*
    public Mapping( String pseudo, String phrase, Boolean isValid){
        this.pseudo = pseudo;
        this.phrase = phrase;
        this.isValid = isValid;
    }
*/

    public BotEntity convertToBotEntity(QuestionDto questionDto){
        /*
        botEntity.setPseudo(pseudo);
        botEntity.setPhrase(phrase);
        botEntity.setIsValid(isValid);
        */
        BotEntity botEntity = new BotEntity();

        botEntity.setPseudo(questionDto.getPseudo());
        botEntity.setPhrase(questionDto.getPhrase());
        botEntity.setIsValid(questionDto.getIsValid());

       return botEntity;
    }

   public QuestionDto convertToQuestioDto(BotEntity botEntity){
       return new QuestionDto(botEntity.getPseudo(), botEntity.getPhrase());
    }

}
