package com.api.bot.model;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data // générer les getteres et setters mis en commenatire ci-dessous
@Component // Ajoute la classe dans le contexte Spring
public class WelcomeModel {

    private String welcomeMessage = "Le serveur TomCat a bien démarré!\nBonjour tous le monde!";

    // Surcharge la méthode toString de la class Object
    @Override
    public String toString() {
        return welcomeMessage;
    }
}
