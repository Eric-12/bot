package com.api.bot.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.ArrayList;

@Data // Annotation Lombok : génére les getters et setters
@Entity (name = "message") // Instance d’une classe qui sera persistante (que l’on pourra sauvegarder dans / charger depuis une base de données relationnelle).
@Table(name = "message") // Indique le nom de la table associée
/*@NamedQueries({
        @NamedQuery(name="findQuestion",query="select u from MessageEntity u where u.phrase = :phrase")
})*/
public class MessageEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    @Id
    // Permet de définir la statégie de génération de la clé lors d'une insertion en base de données.
    // identifiant auto-incrémenté
    private Long id;

    @Column(name = "question")
    private String question;

    @Column(name="reponse")
    private String reponse;

   /* @OneToMany
    private List<BotEntity> botEntityList = new ArrayList<>();*/

}
