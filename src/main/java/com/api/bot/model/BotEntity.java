package com.api.bot.model;

import com.api.bot.service.dto.QuestionDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@Table(name = "bot")
public class BotEntity {


    @Id
    // Permet de définir la statégie de génération de la clé lors d'une insertion en base de données.
    @GeneratedValue(strategy = GenerationType.IDENTITY)  // identifiant auto-incrémenté
    private Long id;

    @Column(name = "pseudo")
    private String pseudo;

    @Column(name = "phrase")
    private String phrase;

    @Column(name = "is_valid")
    private Boolean isValid;

    // @CreatedDate
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @PrePersist
    public void prePersist() {
        createdAt = LocalDateTime.now();

    }

   /* @ManyToOne
    // déclaration d'une table d'association
    @JoinTable(name = "message_bot",
            joinColumns = @JoinColumn(name = "bot_id"),
            inverseJoinColumns = @JoinColumn(name = "message_id"))
    private MessageEntity messageEntity;*/

}
