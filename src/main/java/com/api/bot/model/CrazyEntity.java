package com.api.bot.model;

import lombok.Data;

import javax.persistence.*;

@Data // generated getters setters
@Entity
@Table(name = "crazy_bot")
public class CrazyEntity {
    @Id
    // Permet de définir la statégie de génération de la clé lors d'une insertion en base de données.
    @GeneratedValue(strategy= GenerationType.IDENTITY)  // identifiant auto-incrémenté
    private Long id;

    @Column(name ="reponse")
    private String reponse;
}
