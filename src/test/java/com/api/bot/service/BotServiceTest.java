package com.api.bot.service;

import com.api.bot.model.BotEntity;
import com.api.bot.repository.BotRepository;
import com.api.bot.repository.CrazyRepository;
import org.apache.el.parser.AstSetData;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;


@DisplayName(value = "communication avc le bot")
class BotServiceTest {
    // Déclaration des dépendances de Botservice
    private BotRepository botRepository;
    private MessageService messageService;
    private CrazyRepository crazyRepository;
    private BotEntity botEntity;

    // déclaration de la classe passé en test
    private BotService botService;

    // Méthode exécuté pour chacun des tests
    @BeforeEach
    public void setup() {
        // ici on n'instancie pas les dépendances de classe puisque l'on va les mocker.
        this.crazyRepository = mock(CrazyRepository.class);
        this.botRepository = mock(BotRepository.class);
        this.messageService = mock(MessageService.class);
        //this.botEntity = mock(BotEntity.class);

        // Instancie la classe BotService
        this.botService = new BotService(this.botRepository, this.messageService, this.crazyRepository);
    }

    @Test
    @DisplayName(value = "Test la méthode botService.getAllBot,renvoie tous les enregistrements de la table bot")
    void getAllBot(){
        //Arrange - Organisation
        final ArrayList<BotEntity> botEntityList = new ArrayList<>();
        botEntityList.add(new BotEntity());
        when(this.botRepository.findAll()).thenReturn(botEntityList);

        // Act - action
        final Iterable<BotEntity> allBot = this.botService.getAllBot();

        // Assert - verifier
            // je m'assure que la méthode getAllBot appel bien la methode botRepository.findAll()
        verify(this.botRepository).findAll();
        assertNotNull(allBot);
        assertTrue(allBot.iterator().hasNext());
        Assertions.assertThat(allBot).isNotEmpty();
    }

    @Test
    void getAllBotById(){

        // arrange
        Long id = 1L;

        // act
        final Optional<BotEntity> botEntity = this.botService.getBotById(id);

        // assert
        verify(this.botRepository).findById(id);
        //Assertions.assertThat(botEntity).isNotEmpty();
        //Assertions.assertThat(botEntity).isPresent(); // vérifie si l'objet existes

    }


    /*
    * Etant donné que ...
    * Lorsque ...
    * alors ...
    * */
    @Test
    @DisplayName(value = "récupère un enregistrement de la table bot")
    //void returnOneEntityById() {
     void givenRequestJsonIsNotEmpty_whenGetBotRepositoryFindById_thenReturnOneEntity(){
        // Arrange (organiser)
        final Optional<BotEntity> bot = Optional.of(new BotEntity());
        final Long identifiant = (5L);

        // Act (action) && Assert (vérifier)
        when(this.botRepository.findById(identifiant)).thenReturn(bot);
    }

    @Test
    void  returnTrueIfJsonIsNotEmpty(){

        // Arrange

        // act

        // Assert
    }

    @Test
    void  returnFalseIfJsonIsEmpty(){
        // Arrange

        // act

        // Assert
    }

    /*
    @Test
    void ReturnMessageBotIfQuestionIsEmpty(){
        final QuestionDto questionDto = mock(QuestionDto.class);
        ReponseDto reponseDto = this.botService.replyToMessage(questionDto);

    }
    */
    
}
