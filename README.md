
# Bot Secret


## Description

#### **Introduction**

Le projet consiste à développer un bot capable de répondre contextuellement à des phrases simples.

#### **Contexte**

Ce projet réalisé pendant ma période en entreprise m'a été confié par mon maitre de stage.

#### **Objectifs**

-  M'initier au langage Java,
- apprendre a utiliser Maven,
- développer des tests unitaires avec Junit v5 et Mockito,
- d'utiliser le  Framework de développement Spring boot.

#### **Définition du projet**

Développer une api REST pour communiquer avec le bot, celui-ci doit être capable de répondre à des phrases simples.

Le bot est plutôt lunatique donc il peut aléatoirement répondre par une insulte lorsqu’on lui parle.

Ce projet étant sponsorisé par la NSA, une des fonctionnalités demandées est également de conserver toutes les sollicitations faites à ce bot et de pouvoir facilement les récupérer.

Les données devront être échangées au format Json.

#### **Stack technique**

- Java

- Maven

- Spring 



#### **Fonctionnalités**

Le bot doit être capable de répondre à des sollicitations avec des réponses prédéfinies.

Conserver toutes les sollicitations faites à ce bot et de pouvoir facilement les récupérer.


